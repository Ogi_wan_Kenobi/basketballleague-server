# Basketball League Server
It is a backend part of the Basketball League Project consisting of client part as well (https://bitbucket.org/Ogi_wan_Kenobi/basketballleague-android/src/master/).The idea was to make an app to create and manage basketball leagues. This projects contains entire server side that supports REST web services. It was built in Spring Tool Suite and Hibernate was used to build the MySQL database. The project offers APIs for Android app to consume. The whole project was part of the master thesis.


