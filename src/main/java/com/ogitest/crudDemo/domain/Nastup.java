package com.ogitest.crudDemo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties
public class Nastup {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "ligaId", nullable = false)
	private Liga liga;
	@ManyToOne
	@JoinColumn(name = "timId", nullable = false)
	private Tim tim;
	private int mestoNaTabeli;
	private Boolean uToku;
	private int bodovi;
	private int pobede;
	public Nastup() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Nastup(Long id, Liga liga, Tim tim, int mestoNaTabeli, Boolean uToku, int bodovi, int pobede) {
		super();
		this.id = id;
		this.liga = liga;
		this.tim = tim;
		this.mestoNaTabeli = mestoNaTabeli;
		this.uToku = uToku;
		this.bodovi = bodovi;
		this.pobede = pobede;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Liga getLiga() {
		return liga;
	}
	public void setLiga(Liga liga) {
		this.liga = liga;
	}
	public Tim getTim() {
		return tim;
	}
	public void setTim(Tim tim) {
		this.tim = tim;
	}
	public int getMestoNaTabeli() {
		return mestoNaTabeli;
	}
	public void setMestoNaTabeli(int mestoNaTabeli) {
		this.mestoNaTabeli = mestoNaTabeli;
	}
	public Boolean getuToku() {
		return uToku;
	}
	public void setuToku(Boolean uToku) {
		this.uToku = uToku;
	}
	public int getBodovi() {
		return bodovi;
	}
	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}
	public int getPobede() {
		return pobede;
	}
	public void setPobede(int pobede) {
		this.pobede = pobede;
	}
	
	
}
