package com.ogitest.crudDemo.domain.dto;

public class TimDto {

	private Long id;
	private String ime;
	private Long ligaId;

	public TimDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TimDto(Long id, String ime, Long ligaId) {
		super();
		this.id = id;
		this.ime = ime;
		this.ligaId = ligaId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Long getLigaId() {
		return ligaId;
	}

	public void setLigaId(Long ligaId) {
		this.ligaId = ligaId;
	}

}
