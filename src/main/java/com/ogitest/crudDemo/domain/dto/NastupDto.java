package com.ogitest.crudDemo.domain.dto;

public class NastupDto {

	private Long id;
	private Long ligaId;
	private Long timId;
	private int mestoNaTabeli;
	private Boolean uToku;
	private int bodovi;
	private int pobede;

	public NastupDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NastupDto(Long id, Long ligaId, Long timId, int mestoNaTabeli, Boolean uToku, int bodovi, int pobede) {
		super();
		this.id = id;
		this.ligaId = ligaId;
		this.timId = timId;
		this.mestoNaTabeli = mestoNaTabeli;
		this.uToku = uToku;
		this.bodovi = bodovi;
		this.pobede = pobede;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLigaId() {
		return ligaId;
	}

	public void setLigaId(Long ligaId) {
		this.ligaId = ligaId;
	}

	public Long getTimId() {
		return timId;
	}

	public void setTimId(Long timId) {
		this.timId = timId;
	}

	public int getMestoNaTabeli() {
		return mestoNaTabeli;
	}

	public void setMestoNaTabeli(int mestoNaTabeli) {
		this.mestoNaTabeli = mestoNaTabeli;
	}

	public Boolean getuToku() {
		return uToku;
	}

	public void setuToku(Boolean uToku) {
		this.uToku = uToku;
	}

	public int getBodovi() {
		return bodovi;
	}

	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}

	public int getPobede() {
		return pobede;
	}

	public void setPobede(int pobede) {
		this.pobede = pobede;
	}

}
