package com.ogitest.crudDemo.domain.dto;

import java.util.Date;

public class UtakmicaDto {

	private Long id;
	private Date datum;
	private Long domacinId;
	private Long gostId;
	private int kolo;
	private String rezultat;

	public UtakmicaDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UtakmicaDto(Long id, Date datum, Long domacinId, Long gostId, int kolo, String rezultat) {
		super();
		this.id = id;
		this.datum = datum;
		this.domacinId = domacinId;
		this.gostId = gostId;
		this.kolo = kolo;
		this.rezultat = rezultat;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public Long getDomacinId() {
		return domacinId;
	}

	public void setDomacinId(Long domacinId) {
		this.domacinId = domacinId;
	}

	public Long getGostId() {
		return gostId;
	}

	public void setGostId(Long gostId) {
		this.gostId = gostId;
	}

	public int getKolo() {
		return kolo;
	}

	public void setKolo(int kolo) {
		this.kolo = kolo;
	}

	public String getRezultat() {
		return rezultat;
	}

	public void setRezultat(String rezultat) {
		this.rezultat = rezultat;
	}

}
