package com.ogitest.crudDemo.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties
public class Utakmica {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date datum;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "domacinId", nullable = false)
	private Tim domacin;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "gostId", nullable = false)
	private Tim gost;
	private int kolo;
	private String rezultat;

	public Utakmica() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Utakmica(Long id, Date datum, Tim domacin, Tim gost, int kolo, String rezultat) {
		super();
		this.id = id;
		this.datum = datum;
		this.domacin = domacin;
		this.gost = gost;
		this.kolo = kolo;
		this.rezultat = rezultat;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public Tim getDomacin() {
		return domacin;
	}

	public void setDomacin(Tim domacin) {
		this.domacin = domacin;
	}

	public Tim getGost() {
		return gost;
	}

	public void setGost(Tim gost) {
		this.gost = gost;
	}

	public int getKolo() {
		return kolo;
	}

	public void setKolo(int kolo) {
		this.kolo = kolo;
	}

	public String getRezultat() {
		return rezultat;
	}

	public void setRezultat(String rezultat) {
		this.rezultat = rezultat;
	}

}
