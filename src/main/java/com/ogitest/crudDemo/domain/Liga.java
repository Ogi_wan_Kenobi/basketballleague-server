package com.ogitest.crudDemo.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties
public class Liga {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String ime;
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date datumPocetka;
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date datumZavrsetka;
	private int brojTimova;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId", nullable = false)
	private User user;

	public Liga() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Liga(String ime, Date datumPocetka, Date datumZavrsetka, int brojTimova, User user) {
		super();
		this.ime = ime;
		this.datumPocetka = datumPocetka;
		this.datumZavrsetka = datumZavrsetka;
		this.brojTimova = brojTimova;
		this.user = user;
	}

	public Liga(Long id, String ime, Date datumPocetka, Date datumZavrsetka, int brojTimova, User user) {
		super();
		this.id = id;
		this.ime = ime;
		this.datumPocetka = datumPocetka;
		this.datumZavrsetka = datumZavrsetka;
		this.brojTimova = brojTimova;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Date getDatumPocetka() {
		return datumPocetka;
	}

	public void setDatumPocetka(Date datumPocetka) {
		this.datumPocetka = datumPocetka;
	}

	public Date getDatumZavrsetka() {
		return datumZavrsetka;
	}

	public void setDatumZavrsetka(Date datumZavrsetka) {
		this.datumZavrsetka = datumZavrsetka;
	}

	public int getBrojTimova() {
		return brojTimova;
	}

	public void setBrojTimova(int brojTimova) {
		this.brojTimova = brojTimova;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
