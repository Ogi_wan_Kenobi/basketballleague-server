package com.ogitest.crudDemo.service;

import java.util.Optional;

import com.ogitest.crudDemo.domain.Liga;
import com.ogitest.crudDemo.domain.dto.LigaDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;

public interface LigaService {

	public Liga save(LigaDto ligaDto) throws OgiCustomException;

	public Iterable<Liga> returnAll() throws OgiCustomException;

	public Optional<Liga> returnById(Long Id) throws OgiCustomException;

	public Boolean delete(Long Id) throws OgiCustomException;
}
