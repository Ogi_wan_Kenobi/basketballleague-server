package com.ogitest.crudDemo.service;

import java.util.Optional;

import com.ogitest.crudDemo.domain.Utakmica;
import com.ogitest.crudDemo.domain.dto.UtakmicaDto;

public interface UtakmicaService {

	Utakmica save(UtakmicaDto utakmicaDto);

	Iterable<Utakmica> returnAll();

	Optional<Utakmica> returnById(Long id);

	Boolean delete(Long id);

}
