package com.ogitest.crudDemo.service;

import java.util.Optional;

import com.ogitest.crudDemo.domain.User;
import com.ogitest.crudDemo.domain.dto.UserDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;

public interface UserService {


	public User save(User user) throws OgiCustomException;

	public Boolean delete(Long id);

	public Iterable<User> returnAll();

	public Optional<User> returnById(Long id);

	public Optional<User> returnByUsername(String username);

	public Boolean login(UserDto userDto);

}
