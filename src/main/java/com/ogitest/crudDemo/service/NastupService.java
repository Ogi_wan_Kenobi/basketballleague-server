package com.ogitest.crudDemo.service;

import java.util.Optional;

import com.ogitest.crudDemo.domain.Nastup;
import com.ogitest.crudDemo.domain.dto.NastupDto;

public interface NastupService {

	Nastup save(NastupDto nastupDto);

	Optional<Nastup> returnById(Long id);
	
	

}
