package com.ogitest.crudDemo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ogitest.crudDemo.dao.TimDao;
import com.ogitest.crudDemo.dao.UtakmicaDao;
import com.ogitest.crudDemo.domain.Tim;
import com.ogitest.crudDemo.domain.Utakmica;
import com.ogitest.crudDemo.domain.dto.UtakmicaDto;
import com.ogitest.crudDemo.service.UtakmicaService;

@Service
public class UtakmicaServiceImpl implements UtakmicaService {

	@Autowired
	private UtakmicaDao utakmicaDao;

	@Autowired
	private TimDao timDao;

	@Override
	public Utakmica save(UtakmicaDto utakmicaDto) {

		Long pomId;
		Tim domacinPom = timDao.findById(utakmicaDto.getDomacinId()).get();
		Tim gostPom = timDao.findById(utakmicaDto.getGostId()).get();
		if (utakmicaDto.getId() != null) {
			pomId = utakmicaDto.getId();
		} else {
			pomId = null;
		}
		Utakmica utakmica = new Utakmica(pomId, utakmicaDto.getDatum(), domacinPom, gostPom, utakmicaDto.getKolo(),
				utakmicaDto.getRezultat());
		return utakmicaDao.save(utakmica);
	}

	@Override
	public Iterable<Utakmica> returnAll() {
		return utakmicaDao.findAll();
	}

	@Override
	public Optional<Utakmica> returnById(Long id) {
		return utakmicaDao.findById(id);
	}

	@Override
	public Boolean delete(Long id) {
		utakmicaDao.deleteById(id);
		if (utakmicaDao.existsById(id)) {
			return false;
		} else {
			return true;
		}
	}

}
