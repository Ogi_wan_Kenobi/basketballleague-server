package com.ogitest.crudDemo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ogitest.crudDemo.dao.UserDao;
import com.ogitest.crudDemo.domain.User;
import com.ogitest.crudDemo.domain.dto.UserDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;
import com.ogitest.crudDemo.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	// private static Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserService userService;

	/**
	 * This method will return new User object
	 * 
	 * @return User
	 * @throws OgiCustomException
	 */

	@Override
	public User save(User user) throws OgiCustomException {

		try {
			userDao.save(user);
			return user;
		} catch (Exception e) {
			throw new OgiCustomException("User not created.");
		}
	}

	@Override
	public Boolean delete(Long id) {
		userDao.deleteById(id);
		if (userDao.existsById(id)) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public Iterable<User> returnAll() {
		return userDao.findAll();

	}

	@Override
	public Optional<User> returnById(Long id) {
		return userDao.findById(id);
	}

	@Override
	public Optional<User> returnByUsername(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	public Boolean login(UserDto userDto) {

		Optional<User> userOpt = userService.returnByUsername(userDto.getUsername());
		User user = userOpt.get();
		if (userOpt.isPresent()) {
			if (user.getPassword().equals(userDto.getPassword())) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}

}
