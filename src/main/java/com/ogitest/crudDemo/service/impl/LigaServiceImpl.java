package com.ogitest.crudDemo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ogitest.crudDemo.dao.LigaDao;
import com.ogitest.crudDemo.dao.UserDao;
import com.ogitest.crudDemo.domain.Liga;
import com.ogitest.crudDemo.domain.dto.LigaDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;
import com.ogitest.crudDemo.service.LigaService;

@Service
public class LigaServiceImpl implements LigaService {

	@Autowired
	private LigaDao ligaDao;

	@Autowired
	private UserDao userDao;

	@Override
	public Liga save(LigaDto ligaDto) throws OgiCustomException {

		Optional<com.ogitest.crudDemo.domain.User> userOpt = userDao.findById(ligaDto.getUserId());
		com.ogitest.crudDemo.domain.User user = userOpt.get();
		Long pomId;
		if (ligaDto.getId() != null) {
			pomId = ligaDto.getId();
		} else {
			pomId = null;
		}

		try {
			Liga liga = new Liga(pomId, ligaDto.getIme(), ligaDto.getDatumPocetka(), ligaDto.getDatumZavrsetka(),
					ligaDto.getBrojTimova(), user);
			ligaDao.save(liga);
			return liga;
		} catch (Exception e) {
			throw new OgiCustomException("Liga not saved.");
		}

	}

	@Override
	public Iterable<Liga> returnAll() throws OgiCustomException {
		return ligaDao.findAll();
	}

	@Override
	public Optional<Liga> returnById(Long id) {
		return ligaDao.findById(id);
	}

	@Override
	public Boolean delete(Long id) throws OgiCustomException {
		ligaDao.deleteById(id);
		if (ligaDao.existsById(id)) {
			return false;
		} else {
			return true;
		}
	}

}
