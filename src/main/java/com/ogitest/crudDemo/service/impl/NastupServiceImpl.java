package com.ogitest.crudDemo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ogitest.crudDemo.dao.LigaDao;
import com.ogitest.crudDemo.dao.NastupDao;
import com.ogitest.crudDemo.dao.TimDao;
import com.ogitest.crudDemo.domain.Liga;
import com.ogitest.crudDemo.domain.Nastup;
import com.ogitest.crudDemo.domain.Tim;
import com.ogitest.crudDemo.domain.dto.NastupDto;
import com.ogitest.crudDemo.service.NastupService;

@Service
public class NastupServiceImpl implements NastupService {

	@Autowired
	private NastupDao nastupDao;

	@Autowired
	private LigaDao ligaDao;

	@Autowired
	private TimDao timDao;


	@Override
	public Nastup save(NastupDto nastupDto) {
		
		Long pomId;
		Liga ligaPom = ligaDao.findById(nastupDto.getLigaId()).get();
		Tim timPom = timDao.findById(nastupDto.getTimId()).get();
		
		if(nastupDto.getId()!= null) {
			pomId = nastupDto.getId();
		}else {
			pomId = null;
		}
		Nastup nastup = new Nastup(pomId, ligaPom, timPom, nastupDto.getMestoNaTabeli(), nastupDto.getuToku(), nastupDto.getBodovi(), nastupDto.getPobede());
		return nastupDao.save(nastup);
	}


	@Override
	public Optional<Nastup> returnById(Long id) {
		return nastupDao.findById(id);
	}

}
