package com.ogitest.crudDemo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ogitest.crudDemo.dao.LigaDao;
import com.ogitest.crudDemo.dao.TimDao;
import com.ogitest.crudDemo.domain.Liga;
import com.ogitest.crudDemo.domain.Tim;
import com.ogitest.crudDemo.domain.dto.TimDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;
import com.ogitest.crudDemo.service.TimService;

@Service
public class TimServiceImpl implements TimService {

	@Autowired
	private TimDao timDao;

	@Autowired
	private LigaDao ligaDao;

	@Override
	public Tim save(TimDto timDto) throws OgiCustomException {
		Optional<Liga> ligaOpt = ligaDao.findById(timDto.getLigaId());
		Liga liga = ligaOpt.get();
		Long pomId;
		if (timDto.getId() != null) {
			pomId = timDto.getId();
		} else {
			pomId = null;
		}
		try {
			Tim tim = new Tim(pomId, timDto.getIme(), liga);
			timDao.save(tim);
			return tim;
		} catch (Exception e) {
			throw new OgiCustomException("Liga not saved.");
		}
	}

	@Override
	public Iterable<Tim> returnAll() throws OgiCustomException {
		return timDao.findAll();
	}

	@Override
	public Optional<Tim> returnById(Long id) {
		return timDao.findById(id);
	}

	@Override
	public Boolean delete(Long id) throws OgiCustomException {
		timDao.deleteById(id);
		if (timDao.existsById(id)) {
			return false;
		} else {
			return true;
		}
	}
}
