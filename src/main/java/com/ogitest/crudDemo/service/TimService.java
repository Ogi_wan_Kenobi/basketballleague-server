package com.ogitest.crudDemo.service;

import java.util.Optional;

import com.ogitest.crudDemo.domain.Tim;
import com.ogitest.crudDemo.domain.dto.TimDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;

public interface TimService {

	public Tim save(TimDto timDto) throws OgiCustomException;

	public Iterable<Tim> returnAll() throws OgiCustomException;

	public Optional<Tim> returnById(Long Id) throws OgiCustomException;

	public Boolean delete(Long Id) throws OgiCustomException;

}
