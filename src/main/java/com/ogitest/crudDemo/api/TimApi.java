package com.ogitest.crudDemo.api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ogitest.crudDemo.domain.Tim;
import com.ogitest.crudDemo.domain.dto.TimDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;
import com.ogitest.crudDemo.service.TimService;

@RestController
@RequestMapping("/Tim")
public class TimApi {

	@Autowired
	private TimService timService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Tim save(@RequestBody TimDto timDto) throws OgiCustomException {

		Tim returnTim = timService.save(timDto);
		return returnTim;
	}

	@RequestMapping(value = "/returnAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Iterable<Tim> returnAll() throws OgiCustomException {
		return (Iterable<Tim>) timService.returnAll();

	}

	@RequestMapping(value = "/returnById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<Tim> returnById(@PathVariable Long id) throws OgiCustomException {
		return timService.returnById(id);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody Boolean delete(@PathVariable Long id) throws OgiCustomException {
		return timService.delete(id);
	}
}
