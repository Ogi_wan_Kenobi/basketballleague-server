package com.ogitest.crudDemo.api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ogitest.crudDemo.domain.Liga;
import com.ogitest.crudDemo.domain.dto.LigaDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;
import com.ogitest.crudDemo.service.LigaService;

@RestController
@RequestMapping("/Liga")
public class LigaApi {

	@Autowired
	private LigaService ligaService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Liga save(@RequestBody LigaDto ligaDto) throws OgiCustomException {

		Liga returnLiga = ligaService.save(ligaDto);
		return returnLiga;
	}

	@RequestMapping(value = "/returnAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Iterable<Liga> returnAll() throws OgiCustomException {
		return (Iterable<Liga>) ligaService.returnAll();

	}

	@RequestMapping(value = "/returnById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<Liga> returnById(@PathVariable Long id) throws OgiCustomException {
		return ligaService.returnById(id);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody Boolean delete(@PathVariable Long id) throws OgiCustomException {
		return ligaService.delete(id);
	}
}
