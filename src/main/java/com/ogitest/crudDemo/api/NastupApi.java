package com.ogitest.crudDemo.api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ogitest.crudDemo.domain.Nastup;
import com.ogitest.crudDemo.domain.dto.NastupDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;
import com.ogitest.crudDemo.service.NastupService;

@RestController
@RequestMapping("/Nastup")
public class NastupApi {

	@Autowired
	private NastupService nastupService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Nastup save(@RequestBody NastupDto nastupDto) throws OgiCustomException {

		return nastupService.save(nastupDto);

	}

	@RequestMapping(value = "/returnById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<Nastup> returnById(@PathVariable Long id) throws OgiCustomException {
		return nastupService.returnById(id);

	}

}