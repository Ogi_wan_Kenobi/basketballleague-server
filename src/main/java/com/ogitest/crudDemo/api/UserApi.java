package com.ogitest.crudDemo.api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ogitest.crudDemo.domain.User;
import com.ogitest.crudDemo.domain.dto.UserDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;
import com.ogitest.crudDemo.service.UserService;

@RestController
@RequestMapping("/User")
public class UserApi {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody User save(@RequestBody User user) throws OgiCustomException {
		User pomUser = userService.save(user);
		pomUser.setPassword(null);
		return pomUser;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody Boolean delete(@PathVariable Long id) throws OgiCustomException {
		return userService.delete(id);
	}

	@RequestMapping(value = "/returnAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Iterable<User> returnAll() throws OgiCustomException {
		return (Iterable<User>) userService.returnAll();

	}

	@RequestMapping(value = "/returnById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<User> returnById(@PathVariable Long id) throws OgiCustomException {
		return userService.returnById(id);
	}

	@RequestMapping(value = "/returnByUsername/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<User> returnByUsername(@PathVariable String username) throws OgiCustomException {
		return userService.returnByUsername(username);
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Boolean save(@RequestBody UserDto userDto) throws OgiCustomException {

		return userService.login(userDto);
	}

}
