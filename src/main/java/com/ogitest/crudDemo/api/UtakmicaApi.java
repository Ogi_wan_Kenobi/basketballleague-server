package com.ogitest.crudDemo.api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ogitest.crudDemo.domain.Utakmica;
import com.ogitest.crudDemo.domain.dto.UtakmicaDto;
import com.ogitest.crudDemo.exceptions.OgiCustomException;
import com.ogitest.crudDemo.service.UtakmicaService;

@RestController
@RequestMapping("/Utakmica")
public class UtakmicaApi {

	@Autowired
	private UtakmicaService utakmicaSerivce;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Utakmica save(@RequestBody UtakmicaDto utakmicaDto) throws OgiCustomException {

		return utakmicaSerivce.save(utakmicaDto);

	}

	 @RequestMapping(value = "/returnAll", method = RequestMethod.GET, produces =
	 MediaType.APPLICATION_JSON_VALUE)
	 public @ResponseBody Iterable<Utakmica> returnAll() throws OgiCustomException {
	 return (Iterable<Utakmica>) utakmicaSerivce.returnAll();
	
	 }
	
	 @RequestMapping(value = "/returnById/{id}", method = RequestMethod.GET,
	 produces = MediaType.APPLICATION_JSON_VALUE)
	 public Optional<Utakmica> returnById(@PathVariable Long id) throws
	 OgiCustomException {
	 return utakmicaSerivce.returnById(id);
	 }
	
	 @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	 public @ResponseBody Boolean delete(@PathVariable Long id) throws
	 OgiCustomException {
	 return utakmicaSerivce.delete(id);
	 }
}
