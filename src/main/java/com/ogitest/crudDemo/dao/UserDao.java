package com.ogitest.crudDemo.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ogitest.crudDemo.domain.User;

public interface UserDao extends JpaRepository<User, Long> {

	public Optional<User> findById(Long id);
	
	public Optional<User> findByUsername(String username);
	
}
