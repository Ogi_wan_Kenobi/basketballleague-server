package com.ogitest.crudDemo.dao;

import org.springframework.data.repository.CrudRepository;

import com.ogitest.crudDemo.domain.Utakmica;

public interface UtakmicaDao extends CrudRepository<Utakmica, Long>{

}
