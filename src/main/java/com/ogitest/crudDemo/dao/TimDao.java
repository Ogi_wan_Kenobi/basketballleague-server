package com.ogitest.crudDemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ogitest.crudDemo.domain.Tim;

public interface TimDao extends JpaRepository<Tim, Long>{

}
