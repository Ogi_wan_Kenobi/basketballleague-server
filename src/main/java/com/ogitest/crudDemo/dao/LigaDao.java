package com.ogitest.crudDemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ogitest.crudDemo.domain.Liga;

public interface LigaDao extends JpaRepository<Liga, Long> {

	
}
