package com.ogitest.crudDemo.dao;

import org.springframework.data.repository.CrudRepository;

import com.ogitest.crudDemo.domain.Nastup;

public interface NastupDao extends CrudRepository<Nastup, Long>{

}
