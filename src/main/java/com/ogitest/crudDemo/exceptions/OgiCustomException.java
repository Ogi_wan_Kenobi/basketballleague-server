package com.ogitest.crudDemo.exceptions;

public class OgiCustomException extends Exception {

	private static final long serialVersionUID = -6444569482827347995L;

	private String message;

	public OgiCustomException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OgiCustomException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
